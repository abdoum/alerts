package com.safetynet.alerts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EmptySearchResult extends RuntimeException {

	public EmptySearchResult(String message) {
		super(message);
	}
}

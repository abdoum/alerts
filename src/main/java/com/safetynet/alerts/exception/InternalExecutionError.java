package com.safetynet.alerts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalExecutionError extends RuntimeException{
	public InternalExecutionError(String message) {
		super(message);
	}
}

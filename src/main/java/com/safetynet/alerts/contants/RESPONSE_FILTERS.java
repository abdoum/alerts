package com.safetynet.alerts.contants;

public class RESPONSE_FILTERS {
	public static final String[] CHILD = new String[]{"address", "city", "zip", "phone", "email", "minor", "medicalRecord"};
	public static final String[] FIRESTATION = new String[]{"email", "medicalRecord", "minor", "city", "zip"};
	public static final String[] FLOOD = new String[]{"address", "phone", "fullNameAndMedicalRecords", "age"};
	public static final String[] FLOOD_PERSON = new String[]{"medications", "allergies"};
	public static final String[] OTHER_PERSONS_IN_HOUSE = new String[]{"firstName", "lastName", "age"};
	public static final String[] MEDICATIONS_AND_ALLERGIES_INCLUDE = new String[]{"medications", "allergies"};
	public static final String[] PERSON_INFOS = new String[]{"lastName", "address", "age", "medicalRecord"};
	public static final String[] PERSONS_COVERED = new String[]{"email", "medicalRecord", "minor", "city", "zip", "birthdate", "age", "address"};
	public static final String[] PERSON_INFOS_EXCLUDING = new String[]{"phone", "zip", "city", "minor"};
	public static final String[] NONE = new String[]{""};
	private RESPONSE_FILTERS(){}
}

package com.safetynet.alerts.service;

import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.safetynet.alerts.model.MedicalRecordDTO;
import com.safetynet.alerts.model.PersonDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * The type Helpers service.
 */
@Slf4j
@Service
public class HelpersService {

	/**
	 * Compare two strings boolean.
	 *
	 * @param string1 the string 1
	 * @param string2 the string 2
	 * @return the boolean
	 */
	public boolean compareTwoStrings(String string1, String string2) {
		return string1.trim().equalsIgnoreCase(string2.trim());
	}

	/**
	 * Get full name of person string.
	 *
	 * @param person the person
	 * @return the string
	 */
	public String getFullNameOfPerson(PersonDTO person){
		return person.getFirstName().toLowerCase().trim().concat(person.getLastName().toLowerCase().trim());
	}

	/**
	 * Get full name of medical record string.
	 *
	 * @param medicalRecord the medical record
	 * @return the string
	 */
	public String getFullNameOfMedicalRecord(MedicalRecordDTO medicalRecord){
		return medicalRecord.getFirstName().toLowerCase().trim()
				.concat(medicalRecord.getLastName().toLowerCase().trim());
	}

	/**
	 * Filter results mapping jackson value.
	 *
	 * @param personList             the person list
	 * @param excludeClassName       the exclude class name
	 * @param exclude                the exclude
	 * @param nestedIncludeClassName the nested include class name
	 * @param nestedInclude          the nested include
	 * @return the mapping jackson value
	 */
	public MappingJacksonValue filterResults(List<Object> personList,
											 String excludeClassName,
											 String[] exclude,
											 String nestedIncludeClassName,
											 String[] nestedInclude) {
		var customFilters = new SimpleFilterProvider()
				.addFilter(excludeClassName, serializeAllExcept(exclude))
				.addFilter( nestedIncludeClassName, filterOutAllExcept(nestedInclude));
		var filteredPersons = new MappingJacksonValue(personList);
		filteredPersons.setFilters(customFilters);
		return filteredPersons;
	}

	public MappingJacksonValue filterResultsOfMap(Map<String, Object> personList,
												  String excludeClassName,
												  String[] exclude,
												  String nestedIncludeClassName,
												  String[] nestedInclude) {
		var customFilters = new SimpleFilterProvider()
				.addFilter(excludeClassName, serializeAllExcept(exclude))
				.addFilter( nestedIncludeClassName, filterOutAllExcept(nestedInclude));
		var filteredPersons = new MappingJacksonValue(personList);
		filteredPersons.setFilters(customFilters);
		return filteredPersons;
	}
	/**
	 * Calculates the age for a person.
	 *
	 * @param birthdate 	String the person's birthdate in MM/dd/yyy format
	 * @return int the calculated age
	 */
	public Integer calculateAge(String birthdate) {
		int age;
		try {
			var diff = Period.between(LocalDate.parse(birthdate, DateTimeFormatter.ofPattern("MM/dd/yyyy")), LocalDate.now());
			age = diff.getYears();
			return age;
		} catch (Exception e) {
			log.warn("no medical record found for " + birthdate);
			return null;
		}
	}

	/**
	 * Serialize all except simple bean property filter.
	 *
	 * @param exclude the exclude
	 * @return the simple bean property filter
	 */
	private SimpleBeanPropertyFilter serializeAllExcept(String[] exclude) {
		return SimpleBeanPropertyFilter.serializeAllExcept(StringUtils.arrayToCommaDelimitedString(exclude).split(","));
	}

	/**
	 * Filter out all except simple bean property filter.
	 *
	 * @param nestedInclude the nested include
	 * @return the simple bean property filter
	 */
	private SimpleBeanPropertyFilter filterOutAllExcept(String[] nestedInclude) {
		return SimpleBeanPropertyFilter.filterOutAllExcept(StringUtils.arrayToCommaDelimitedString(nestedInclude).split(","));
	}
}

package com.safetynet.alerts.service;

import com.safetynet.alerts.contants.RESPONSE_FILTERS;
import com.safetynet.alerts.exception.InvalidArgument;
import com.safetynet.alerts.model.FirestationDTO;
import com.safetynet.alerts.model.PersonDTO;
import com.safetynet.alerts.repository.IFireStationDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class FirestationService {

	@Autowired
	IFireStationDao iFireStationDao;

	@Autowired
	PersonService personService;

	@Autowired
	HelpersService helpersService;

	/**
	 * Add firestation firestation dto.
	 *
	 * @param firestation the firestation
	 * @return the firestation dto
	 */
	public FirestationDTO addFirestation(FirestationDTO firestation) {
		log.debug(String.valueOf(firestation));
		var persistentFirestation = new FirestationDTO();
		persistentFirestation.setStation(firestation.getStation());
		persistentFirestation.setAddress(firestation.getAddress());
		FirestationDTO savedFirestation = iFireStationDao.saveAndFlush(persistentFirestation);
		log.debug(String.valueOf(savedFirestation));
		return savedFirestation;
	}

	/**
	 * Update firestation firestation dto.
	 *
	 * @param firestation the firestation
	 * @return the firestation dto
	 */
	public FirestationDTO updateFirestation(FirestationDTO firestation) {
		log.debug(String.valueOf(firestation));
		var persistentFirestation = new FirestationDTO();
		persistentFirestation.setStation(firestation.getStation());
		persistentFirestation.setAddress(firestation.getAddress());
		if (firestation.getAddress().trim().isBlank() || firestation.getStation().trim().isBlank()) {
			throw new InvalidArgument("The provided data contains empty fields.");
		}
		String address = persistentFirestation.getAddress();
		FirestationDTO existingFirestation = iFireStationDao.getOneByAddress(address);
		FirestationDTO updatedFirestation;
		if (existingFirestation == null){
			throw new InvalidArgument("no firestation with the following address was found : " + firestation.getAddress());
		}else{
			existingFirestation.setStation(persistentFirestation.getStation());
			updatedFirestation =	iFireStationDao.saveAndFlush(existingFirestation);
		}
		log.debug(String.valueOf(updatedFirestation));
		return updatedFirestation;
	}

	/**
	 * Delete firestation.
	 *
	 * @param firestation the firestation
	 */
	public void deleteFirestation(FirestationDTO firestation) {
		log.debug(String.valueOf(firestation));
		var persistentFirestation = new FirestationDTO();
		persistentFirestation.setStation(firestation.getStation());
		persistentFirestation.setAddress(firestation.getAddress());
		FirestationDTO existingFirestation = iFireStationDao.getOneByStationAndAddress(
				persistentFirestation.getStation()
				, persistentFirestation.getAddress());
		if (existingFirestation == null) {
			log.debug("existingFirestation is null");
			throw new InvalidArgument("no firestation with id " + persistentFirestation.getStation() + " were found.");
		}
		log.debug("Firestation successfully deleted.");
		iFireStationDao.deleteById(existingFirestation.getId());
	}

	/**
	 * Find covered persons by firestation number mapping jackson value.
	 *
	 * @param stationNumber the station number
	 * @return the mapping jackson value
	 */
	public MappingJacksonValue findCoveredPersonsByFirestationNumber(String stationNumber) {
		log.debug(String.valueOf(stationNumber));
		if (stationNumber.isBlank()){
			throw new InvalidArgument("please provide a valid firestation number");
		}
		List<String> addresses = iFireStationDao.retrieveAddressesByStation(stationNumber);
		Map<String, Object> persons = personService.findPersonsByAddress(addresses);
		var filteredPersons = helpersService.filterResultsOfMap(persons,
				"person", RESPONSE_FILTERS.PERSONS_COVERED,
				"otherPersonsInTheHouse", RESPONSE_FILTERS.NONE );
		log.debug(String.valueOf(filteredPersons));
		return filteredPersons;
	}

//	/**
//	 * Find persons covered by station map.
//	 *
//	 * @param stationNumber the station number
//	 * @return the map
//	 */
//	public Map<String, Object> findPersonsCoveredByStation(String stationNumber) {
//		List<PersonDTO> personsList = iPersonDao.findAll();
//		List<Firestation> firestationList = findCoveredAddresses(stationNumber);
//		Set<PersonDTO> personsCovered = new HashSet<>();
//		Map<String, Object> result = new HashMap<>();
//		if (!firestationList.isEmpty()) {
//			personsCovered.addAll(findCoveredPersonsByFirestations(personsList, firestationList));
//			personsCovered = personsCovered.stream().map(person -> {
//				person.setMedicalRecord(iMedicalRecordDao.getById(person.getId()));
//				return person;
//			}).collect(Collectors.toSet());
//		}
//		result.put("persons", personsCovered);
//		return result;
//	}
//
//	/**
//	 * Find covered addresses list.
//	 *
//	 * @param stationNumber the station number
//	 * @return the list
//	 */
//	private List<Firestation> findCoveredAddresses(String stationNumber) {
//		List<Firestation> firestationList = new ArrayList<>();
//		for (Firestation firestation1 : firestationDao.getFirestations()) {
//			if (firestation1.getStation().equals(stationNumber)) {
//				firestationList.add(firestation1);
//			}
//		}
//		return firestationList;
//	}
//
//	/**
//	 * Find assigned firestations by address.
//	 *
//	 * @param address the address
//	 * @return the list of assigned firestations for the given address
//	 */
//	public List<Firestation> findAssignedFirestationsByAddress(String address) {
//		List<Firestation> firestationList = new ArrayList<>();
//		for (Firestation fireSt : firestationDao.getFirestations()) {
//			if (fireSt.getAddress().equals(address)) {
//				firestationList.add(fireSt);
//			}
//		}
//		return firestationList;
//	}


	/**
	 * Find covered persons list.
	 *
	 * @param personsList     the persons list
	 * @param firestationList the firestation list
	 * @return the list of found persons
	 */
	public Set<PersonDTO> findCoveredPersonsByFirestations(List<PersonDTO> personsList, List<FirestationDTO> firestationList) {
		Set<PersonDTO> personsCovered = new HashSet<>();
		for (FirestationDTO firestation : firestationList) {
			List<PersonDTO> foundPersons = personsList.stream().filter(person ->
					helpersService.compareTwoStrings(person.getAddress(), firestation.getAddress())).collect(Collectors.toList());
			personsCovered.addAll(foundPersons);
		}
		return personsCovered;
	}

	/**
	 * Find firestations by number.
	 *
	 * @param firestationNumber the firestation number
	 * @return the list of found firestations
	 */
	public List<FirestationDTO> findFirestationsByNumber(String firestationNumber){
		return iFireStationDao.getAllByStation(firestationNumber);
	}
//	/**
//	 * Find covered persons by firestation number.
//	 *
//	 * @param firestationNumber the firestation number
//	 * @return the list of persons covered by the firestation
//	 */
//	public List<PersonDTO> findCoveredPersonsByFirestationNumber(String firestationNumber) {
//		List<PersonDTO> personsList = iPersonDao.findAll();
//		List<PersonDTO> personsCovered = new ArrayList<>();
//		for (Firestation firestation : firestationDao.getFirestations()) {
//			Stream<PersonDTO> foundPersons = personsList.stream().filter(person ->
//					helpersService.compareTwoStrings(person.getAddress(), firestation.getAddress()));
//			personsCovered.addAll(0, foundPersons.collect(Collectors.toList()));
//		}
//		return personsCovered;
//	}
}

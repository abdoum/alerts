package com.safetynet.alerts.service;

import com.safetynet.alerts.exception.EmptySearchResult;
import com.safetynet.alerts.exception.InvalidArgument;
import com.safetynet.alerts.model.FireDTO;
import com.safetynet.alerts.repository.IFireDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class FireService {

	@Autowired
	IFireDao iFireDao;

	/**
	 * Find residents and firestation by address.
	 *
	 * @param address the address to find residents and firestations for
	 * @return the list of found residents and firestations
	 */
	public List<FireDTO> findResidentsAndFirestationByAddress(String address) {
		log.debug(String.valueOf(address));
		if (address.isBlank()) {
			throw new InvalidArgument("please provide a valid address");
		}
		var persons = iFireDao.getAllByAddress(address.trim());
		if (persons == null) {
			throw new EmptySearchResult("no stations found for address: "+ address.trim());
		}
		log.debug(String.valueOf(persons));
		return persons;
	}
}

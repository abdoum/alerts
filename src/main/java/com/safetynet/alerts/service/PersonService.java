package com.safetynet.alerts.service;

import com.safetynet.alerts.contants.RESPONSE_FILTERS;
import com.safetynet.alerts.exception.EmptySearchResult;
import com.safetynet.alerts.exception.InvalidArgument;
import com.safetynet.alerts.model.ChildAndPersonsInHouse;
import com.safetynet.alerts.model.FirestationDTO;
import com.safetynet.alerts.model.PersonDTO;
import com.safetynet.alerts.repository.IPersonDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PersonService {

	@Autowired
	FirestationService firestationService;

	@Autowired
	IPersonDao iPersonDao;

	@Autowired
	HelpersService helpersService;


	@Autowired
	MedicalRecordService medicalRecordService;


	/**
	 * Delete person.
	 *
	 * @param fullNameOfPerson the full name of person
	 */
	public void deletePerson(String fullNameOfPerson) {
		log.debug(String.valueOf(fullNameOfPerson));
		if (fullNameOfPerson.trim().isBlank()) {
			throw new EmptySearchResult("the fullName of the person to delete must be provided");
		}
		PersonDTO foundPerson = iPersonDao.getOneByFirstNameAndLastName(fullNameOfPerson.trim());
		if (foundPerson != null) {
			iPersonDao.deleteById(foundPerson.getId());
			log.debug(String.valueOf(foundPerson));
		}
	}

	/**
	 * Update person person dto.
	 *
	 * @param person the person
	 * @return the person dto
	 */
	public PersonDTO updatePerson(PersonDTO person) {
		log.debug(String.valueOf(person));
		String fullName = person.getFullName();
		PersonDTO existingPerson = iPersonDao.getOneByFirstNameAndLastName(fullName);
		if (existingPerson == null) {
			log.debug("person not found in database");
			throw new EmptySearchResult("Could not find " + person.getFirstName() + " " + person.getLastName() );
		}
		existingPerson.setFirstName(person.getFirstName());
		existingPerson.setLastName(person.getLastName());
		existingPerson.setAddress(person.getAddress());
		existingPerson.setCity(person.getCity());
		existingPerson.setMedicalRecord(person.getMedicalRecord());
		existingPerson.setPhone(person.getPhone());
		existingPerson.setZip(person.getZip());
		existingPerson.setEmail(person.getEmail());
		PersonDTO savedPerson = iPersonDao.saveAndFlush(existingPerson);
		log.debug(String.valueOf(savedPerson));
		return savedPerson;
	}

	/**
	 * Add person person dto.
	 *
	 * @param person the person
	 * @return the person dto
	 */
	public PersonDTO addPerson(PersonDTO person) {
		log.debug(person.toString());
		var persistentPerson = new PersonDTO();
		persistentPerson.setFirstName(person.getFirstName());
		persistentPerson.setLastName(person.getLastName());
		persistentPerson.setAddress(person.getAddress());
		persistentPerson.setCity(person.getCity());
		persistentPerson.setMedicalRecord(person.getMedicalRecord());
		persistentPerson.setPhone(person.getPhone());
		persistentPerson.setZip(person.getZip());
		persistentPerson.setEmail(person.getEmail());

		PersonDTO savedPerson = iPersonDao.saveAndFlush(persistentPerson);
		log.debug(savedPerson.toString());
		return savedPerson;
	}

	/**
	 * Find persons by last name.
	 *
	 * @param firstNameOfPerson the first name of person
	 * @param lastNameOfPerson  the last name of person
	 * @return the list of found persons
	 */
	public List<Object> findPersonsByFullName(String firstNameOfPerson, String lastNameOfPerson){
		List<PersonDTO> personList = iPersonDao.findAll();
		List<PersonDTO> result = new ArrayList<>();
		if (!firstNameOfPerson.isBlank()){
			result.addAll(personList.stream()
					.filter(person ->
							helpersService.compareTwoStrings(person.getFirstName(), firstNameOfPerson))
					.collect(Collectors.toList()));
		}
		if (!lastNameOfPerson.isBlank()){
			result.addAll(personList.stream()
					.filter(person ->
							helpersService.compareTwoStrings(person.getLastName(), lastNameOfPerson))
					.collect(Collectors.toList()));
		}
		return result.stream().map(person -> {
					person.setMedicalRecord(medicalRecordService.findMedicalRecordByPerson(person));
					return person;
				}
		).collect(Collectors.toList());
	}

	/**
	 * Gets all children for a given address.
	 *
	 * @param address String the address to look for
	 * @return all children by address or an empty array if no children are found
	 */
	public MappingJacksonValue getAllChildrenByAddress(String address) {
		if (address.isBlank()) {
			throw new InvalidArgument("please provide a valid address");
		}
		Set<ChildAndPersonsInHouse> personList = findChildrenAndTheirFamilyByAddress(address);
		List<Object> mainList = new ArrayList<>(personList);
		return helpersService.filterResults(mainList,
				"person", RESPONSE_FILTERS.CHILD,
				"otherPersonsInTheHouse", RESPONSE_FILTERS.OTHER_PERSONS_IN_HOUSE );
	}

	/**
	 * Find children and their family by address list.
	 *
	 * @param address the address
	 * @return the list
	 */
	public Set<ChildAndPersonsInHouse> findChildrenAndTheirFamilyByAddress(String address) {
		List<PersonDTO> foundPersons = findByAddress(address);
		List<ChildAndPersonsInHouse> result = new ArrayList<>();
		for (PersonDTO person : foundPersons) {
			var child  = new ChildAndPersonsInHouse();
			person.setMedicalRecord(medicalRecordService.findMedicalRecordByPerson(person));
			if (helpersService.calculateAge(person.getMedicalRecord().getBirthdate()) <= 18) {
				child.setFirstName(person.getFirstName());
				child.setLastName(person.getLastName());
				child.setOtherPersonsInTheHouse(findOtherPersonsInTheHouse(person));
				result.add(child);
			}
		}
		return new HashSet<>(result);
	}

	/**
	 * Gets residents emails for a given city.
	 *
	 * @param cityName the city name
	 * @return the resident's emails for a city
	 */
	public List<String> getPersonsEmailsForACity(String cityName) {
		log.debug(cityName);
		if (cityName.isBlank()) {
			throw new InvalidArgument("city name should not be empty");
		}
		var emails = iPersonDao.getAllByCity(cityName.trim());
		if (emails.isEmpty()) {
			throw new EmptySearchResult("no emails found for city: " + cityName.trim());
		}
		log.debug(String.valueOf(emails));
		return emails;
	}

	/**
	 * Find persons using an address.
	 *
	 * @param address the address used to find persons
	 * @return the list of found persons for that address
	 */
	public List<PersonDTO> findByAddress(String address)  {
		List<PersonDTO> personList = iPersonDao.findAll();
		return personList.stream().filter(pers ->
				pers.getAddress().equals(address)).collect(Collectors.toList());
	}

	private List<PersonDTO> findOtherPersonsInTheHouse(PersonDTO person) {
		List<PersonDTO> foundPersonsInTheHouse = findByAddress(person.getAddress());
		foundPersonsInTheHouse.removeIf(pers -> pers.getFullName().equals(person.getFullName()));
		return foundPersonsInTheHouse;
	}

	/**
	 * Find persons by address map.
	 *
	 * @param addresses the addresses
	 * @return the map
	 */
	public Map<String, Object> findPersonsByAddress(List<String> addresses){
		var result = new HashMap<String, Object>();
		var persons = iPersonDao.getByAddressIn(addresses);
		result.put("persons", persons);
		int adultsCount = (int) persons.stream().filter(personDTO -> helpersService.calculateAge(
				personDTO.getMedicalRecord() != null ? personDTO.getMedicalRecord().getBirthdate() : null) > 18).count();
		result.put("adultsCount", adultsCount);
		int childrenCount = (int) persons.stream().filter(personDTO -> helpersService.calculateAge(
				personDTO.getMedicalRecord() != null ? personDTO.getMedicalRecord().getBirthdate() : null) <= 18).count();
		result.put("childrenCount", childrenCount);
		return result;
	}

	/**
	 * Gets all phones by station number.
	 *
	 * @param firestationNumber the firestation number
	 * @return the all phones by station number
	 */
	public Set<String> getAllPhonesByStationNumber(String firestationNumber) {
		if (firestationNumber.isBlank()){
			throw new InvalidArgument("please provide a valid firestation number");
		}
		List<FirestationDTO> firestations = firestationService.findFirestationsByNumber(firestationNumber);
		if (firestations.isEmpty()) {
			throw new EmptySearchResult("no firestation found with the provided number");
		}
		Set<PersonDTO> personsCovered = firestationService.findCoveredPersonsByFirestations(iPersonDao.findAll(), firestations);
		if (personsCovered.isEmpty()) {
			throw new EmptySearchResult("no persons found for the address matching the provided fireStation number");
		}
		return personsCovered.stream().map(PersonDTO::getPhone).collect(Collectors.toSet());
	}
}

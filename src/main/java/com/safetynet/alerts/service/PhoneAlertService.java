package com.safetynet.alerts.service;

import com.safetynet.alerts.exception.EmptySearchResult;
import com.safetynet.alerts.exception.InvalidArgument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * The type Phone alert service.
 */
@Service
public class PhoneAlertService {

	@Autowired
	PersonService personService;

	/**
	 * Find phone numbers by station number.
	 *
	 * @param firestationNumber String the firestation number
	 * @return Set of phone numbers for the persons living in the given firestation coverage
	 */
	public Set<String> findPhoneNumbersByStationNumber(String firestationNumber) {
		if (firestationNumber.isBlank()) {
			throw new InvalidArgument("please provide a valid firestation number");
		}
		Set<String> phoneNumbers = personService.getAllPhonesByStationNumber(firestationNumber);
		if (phoneNumbers.isEmpty()) {
			throw new EmptySearchResult("No residents matching the address for the provided firestation number were found");
		}
		return phoneNumbers;
	}


}

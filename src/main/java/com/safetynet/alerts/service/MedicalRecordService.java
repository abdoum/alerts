package com.safetynet.alerts.service;

import com.safetynet.alerts.exception.EmptySearchResult;
import com.safetynet.alerts.exception.InvalidArgument;
import com.safetynet.alerts.model.MedicalRecordDTO;
import com.safetynet.alerts.model.PersonDTO;
import com.safetynet.alerts.repository.IMedicalRecordDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MedicalRecordService {

	@Autowired
	IMedicalRecordDao iMedicalRecordDao;

	/**
	 * Find medical record by person medical record.
	 *
	 * @param person the person
	 * @return the medical record
	 */
	public MedicalRecordDTO findMedicalRecordByPerson(PersonDTO person) {
		return iMedicalRecordDao.getById(person.getId());
	}

	/**
	 * Delete medical record.
	 *
	 * @param medicalRecordToDeleteFullName the medical record to delete full name
	 */
	public void deleteMedicalRecord(String medicalRecordToDeleteFullName) {
		log.debug(String.valueOf(medicalRecordToDeleteFullName));
		if (medicalRecordToDeleteFullName.isBlank()) {
			throw new InvalidArgument("please provide the full name of the medical record to be deleted");
		}
		MedicalRecordDTO foundMedicalRecord = iMedicalRecordDao.getOneByFirstNameAndLastName(medicalRecordToDeleteFullName.trim());
		if (foundMedicalRecord == null) {
			throw new EmptySearchResult("No medical record found");
		}
			iMedicalRecordDao.deleteById(foundMedicalRecord.getId());
	}

	/**
	 * Update medical record medical record dto.
	 *
	 * @param medicalRecord the medical record
	 * @return the medical record dto
	 */
	public MedicalRecordDTO updateMedicalRecord(MedicalRecordDTO medicalRecord) {
		log.debug(medicalRecord.toString());
		String fullName = medicalRecord.getFullName().trim();
		MedicalRecordDTO existingMedicalRecord = iMedicalRecordDao.getOneByFirstNameAndLastName(fullName);
		if (existingMedicalRecord == null) {
			log.debug("no existing medical record found in database");
			throw new EmptySearchResult("Could not find " + medicalRecord.getFirstName() + " " + medicalRecord.getLastName() );
		}
		existingMedicalRecord.setFirstName(medicalRecord.getFirstName());
		existingMedicalRecord.setLastName(medicalRecord.getLastName());
		return iMedicalRecordDao.saveAndFlush(medicalRecord);
	}

	/**
	 * Add medical record medical record dto.
	 *
	 * @param medicalRecord the medical record
	 * @return the medical record dto
	 */
	public MedicalRecordDTO addMedicalRecord(MedicalRecordDTO medicalRecord) {
		log.debug(medicalRecord.toString());
		if (medicalRecord.getFirstName().isBlank() || medicalRecord.getLastName().isBlank()) {
			throw new InvalidArgument("A medical firstname and lastname must be provided must be provided.");
		}
		MedicalRecordDTO addedRecord = iMedicalRecordDao.saveAndFlush(medicalRecord);
		log.debug(String.valueOf(addedRecord));
		return addedRecord;
	}
}

package com.safetynet.alerts.service;

import com.safetynet.alerts.exception.EmptySearchResult;
import com.safetynet.alerts.exception.InvalidArgument;
import com.safetynet.alerts.model.FloodItemDTO;
import com.safetynet.alerts.model.FloodItemWrapper;
import com.safetynet.alerts.repository.IFloodDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.util.stream.Collectors.groupingBy;

@Slf4j
@Service
public class FloodService {

	@Autowired
	IFloodDao iFloodDao;

	/**
	 * Get all by firestation station number list.
	 *
	 * @param firestationNumbers the addresses
	 * @return the list of residents matching addresses of the provided firestations numbers
	 */
	private List<FloodItemWrapper> getAllByFirestationStations(List<String> firestationNumbers){
		List<FloodItemDTO> results = iFloodDao.getAllByFireStationsIn(firestationNumbers);
			List<FloodItemWrapper> floods = new ArrayList<>();
		Map<String, List<FloodItemDTO>> grouped = results.stream()
				.collect(groupingBy(FloodItemDTO::getAddress));
		for (Map.Entry<String, List<FloodItemDTO>> group : grouped.entrySet()) {
			var flood = new FloodItemWrapper();
			flood.setAddress( group.getKey());
				flood.setPersons(group.getValue());
			floods.add(flood);
		}
		return floods;
	}

	/**
	 * Find covered houses list.
	 *
	 * @param listOfFirestationNumbers the list of firestation numbers
	 * @return the list of covered residents
	 */
	public List<FloodItemWrapper> findCoveredResidents(List<String> listOfFirestationNumbers) {
		if (listOfFirestationNumbers.isEmpty()) {
			throw new InvalidArgument("please provide a list of firestation numbers");
		}
		List<FloodItemWrapper> foundHouses = getAllByFirestationStations(listOfFirestationNumbers);
		if (foundHouses.isEmpty()) {
			throw new EmptySearchResult("no results found for stations numbers : " + listOfFirestationNumbers);
		}
		log.debug(String.valueOf(foundHouses));
		return foundHouses;
	}
}

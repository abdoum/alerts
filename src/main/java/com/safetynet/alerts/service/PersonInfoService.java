package com.safetynet.alerts.service;

import com.safetynet.alerts.contants.RESPONSE_FILTERS;
import com.safetynet.alerts.exception.EmptySearchResult;
import com.safetynet.alerts.exception.InvalidArgument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PersonInfoService {
	@Autowired
	PersonService personService;

	@Autowired
	HelpersService helpersService;

	public MappingJacksonValue findPersonsByFullName(Map<String, String> allParams) {
		String firstName = allParams.get("firstName");
		String lastName = allParams.get("lastName");
		if (lastName.isBlank() || firstName.isBlank()) {
			throw new InvalidArgument("please provide a valid firstname and  lastname");
		}
		List<Object> personList = personService.findPersonsByFullName(firstName, lastName);
		if (personList.isEmpty()) {
			throw new EmptySearchResult("No persons found");
		}
		return helpersService.filterResults(personList,
				"person",	RESPONSE_FILTERS.PERSON_INFOS_EXCLUDING,
				"medicalRecord",RESPONSE_FILTERS.MEDICATIONS_AND_ALLERGIES_INCLUDE
		);
	}
}

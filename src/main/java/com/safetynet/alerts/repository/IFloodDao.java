package com.safetynet.alerts.repository;

import com.safetynet.alerts.model.FloodItemDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IFloodDao extends JpaRepository<FloodItemDTO, Long> {
	@Query(value ="SELECT\n" +
			"\tp.id,\n" +
			"\tp.address,\n" +
			"\tCONCAT(p.first_name, ' ', p.last_name, ' ', '(allergies: ', GROUP_CONCAT(DISTINCT CONCAT('', IFNULL(a.allergies, ''))), ', ', 'medications: ', GROUP_CONCAT(DISTINCT CONCAT('', IFNULL(mm.medications, ''))), ')') AS full_name_and_medical_records,\n" +
			"\tp.phone,\n" +
			"\tYEAR(CURRENT_TIMESTAMP) - YEAR(p.birthdate) - (\n" +
			"\tRIGHT(CURRENT_TIMESTAMP, 5) <\n" +
			"\tRIGHT(p.birthdate, 5)) AS age\n" +
			"FROM\n" +
			"\tPERSON p\n" +
			"\tJOIN medical_record m ON m.id = p.id\n" +
			"\tLEFT JOIN medical_recorddto_allergies a ON m.id = a.medical_recorddto_id\n" +
			"\tLEFT JOIN medical_recorddto_medications mm ON m.id = mm.medical_recorddto_id\n" +
			"WHERE\n" +
			"\tp.address IN(\n" +
			"\t\tSELECT\n" +
			"\t\t\taddress FROM firestation f\n" +
			"\t\tWHERE\n" +
			"\t\t\tf.station in(?1))\n" +
			"GROUP BY\n" +
			"\t1\n" +
			"ORDER BY\n" +
			"\taddress",
			nativeQuery =true)
	List<FloodItemDTO> getAllByFireStationsIn(List<String> stations);
}

package com.safetynet.alerts.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.safetynet.alerts.configuration.CustomProperties;
import com.safetynet.alerts.model.AppData;
import com.safetynet.alerts.model.MedicalRecordDTO;
import com.safetynet.alerts.model.PersonDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class MainDataDao {

	@Autowired
	IFireStationDao iFireStationDao;

	@Autowired
	IMedicalRecordDao iMedicalRecordDao;

	@Autowired
	CustomProperties customProperties;

	@Autowired
	IPersonDao iPersonDao;

	/**
	 * Gets data from remote url and saves it in the database.
	 *
	 * @throws JsonProcessingException the json processing exception
	 */
	public void getData() throws JsonProcessingException {
		String baseApiUrl = customProperties.getApiUrl();
		var restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(
				baseApiUrl,
				String.class
		);
		ObjectMapper mapper = new ObjectMapper()
				.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		AppData dataResult = mapper.readValue(
				result, new TypeReference<>() {
				}
		);
		iFireStationDao.saveAllAndFlush(dataResult.getFirestations());

		List<PersonDTO> persons = dataResult.getPersons();

		iPersonDao.saveAllAndFlush(persons);

		for (PersonDTO personDTO : persons) {
			Optional<MedicalRecordDTO> medicalRecord = dataResult.getMedicalrecords().stream().filter(medicalRecordDTO ->
					StringUtils.capitalize(medicalRecordDTO.getFirstName().trim()).concat(
							StringUtils.capitalize(medicalRecordDTO.getLastName().trim())).equalsIgnoreCase(personDTO.getFullName())
			).collect(Collectors.toList()).stream().findFirst();
			if (medicalRecord.isPresent()) {
				personDTO.setMedicalRecord(medicalRecord.orElse(null));
				personDTO.setBirthdate(LocalDate.parse(
						medicalRecord.get().getBirthdate(), DateTimeFormatter.ofPattern("MM/dd/yyyy")));
			}
		}
		iMedicalRecordDao.saveAllAndFlush(dataResult.getMedicalrecords());
		iPersonDao.saveAllAndFlush(persons);
	}
}

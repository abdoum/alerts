package com.safetynet.alerts.repository;

import com.safetynet.alerts.model.FireDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IFireDao extends JpaRepository<FireDTO, Long> {
	@Query(value =
			"SELECT\n" +
					"\tCONCAT(p.first_name, ' ', p.last_name) AS full_name,\n" +
					"\tp.phone,\n" +
					"\tYEAR(CURRENT_TIMESTAMP) - YEAR(p.birthdate) - (\n" +
					"\tRIGHT(CURRENT_TIMESTAMP, 5) <\n" +
					"\tRIGHT(p.birthdate, 5)) AS age,\n" +
					"CONCAT('(allergies: ', a.allergies, ', ', 'medications: ', mm.medications, ')') AS medical_record,\n" +
					"GROUP_CONCAT(DISTINCT f.station) AS station, p.id, medical_record_id\n" +
					"FROM\n" +
					"\tPERSON p\n" +
					"\tJOIN medical_record m ON m.id = p.id\n" +
					"\tJOIN firestation f ON f.address = p.address\n" +
					"\tJOIN medical_recorddto_allergies a ON m.id = a.medical_recorddto_id\n" +
					"\tJOIN medical_recorddto_medications mm ON m.id = mm.medical_recorddto_id\n" +
					"WHERE\n" +
					"\tp.address = ?1\n" ,
			nativeQuery =true)
	List<FireDTO> getAllByAddress(String address);
}

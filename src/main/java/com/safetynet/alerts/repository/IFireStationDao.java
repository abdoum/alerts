package com.safetynet.alerts.repository;

import com.safetynet.alerts.model.FirestationDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IFireStationDao extends JpaRepository<FirestationDTO, Long> {

	List<FirestationDTO> getAllByStation(String station);
	FirestationDTO getOneByAddress(String address);

	@Query(value ="SELECT address FROM FIRESTATION f WHERE f.station = ?1", nativeQuery =true)
	List<String> retrieveAddressesByStation(@Param("station") String station);

	@Query(value ="SELECT station, address FROM FIRESTATION f WHERE f.station = ?1 AND f.address = ?2", nativeQuery =true)
	FirestationDTO getOneByStationAndAddress(@Param("station") String station,
											 @Param("address") String address);
}

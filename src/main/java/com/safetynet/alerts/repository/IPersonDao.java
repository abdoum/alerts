package com.safetynet.alerts.repository;

import com.safetynet.alerts.model.PersonDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface IPersonDao  extends JpaRepository<PersonDTO, Long> {

	@Query(value ="SELECT * FROM PERSON p WHERE CONCAT(p.first_name, p.last_name) = ?1", nativeQuery =true)
	PersonDTO getOneByFirstNameAndLastName(@Param("fullName") String fullName);

	@Query(value ="SELECT first_name, last_name, age, address FROM PERSON p WHERE p.minor  = ?1 AND p.address = ?2 " +
			"GROUP BY address, age, first_name, last_name", nativeQuery =true)
	List<PersonDTO> getAllByMinorAndAddress(@Param("fullName") Boolean minor,
										   @Param("address") String address);

	@Query(value ="SELECT * FROM PERSON p WHERE p.address IN(?1) ORDER BY p.address",
			nativeQuery =true)
	List<PersonDTO> getByAddressIn(@Param("addresses") Collection<String> addresses);

	@Query(value ="SELECT DISTINCT email FROM PERSON p WHERE p.city = ?1 ORDER BY email",
			nativeQuery =true)
	List<String> getAllByCity(String city);
}

package com.safetynet.alerts.repository;

import com.safetynet.alerts.model.MedicalRecordDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IMedicalRecordDao  extends JpaRepository<MedicalRecordDTO, Long> {

	@Query(value ="SELECT * FROM MEDICAL_RECORD m WHERE CONCAT(m.first_name, m.last_name) = ?1", nativeQuery =true)
	MedicalRecordDTO getOneByFirstNameAndLastName(@Param("fullName") String fullName);

	void deleteById(Integer id);
}

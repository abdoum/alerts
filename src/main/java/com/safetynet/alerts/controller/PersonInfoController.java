package com.safetynet.alerts.controller;

import com.safetynet.alerts.exception.EmptySearchResult;
import com.safetynet.alerts.service.PersonInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class PersonInfoController {

	@Autowired
	PersonInfoService personInfoService;

	/**
	 * Find persons by full name mapping.
	 *
	 * @param allParams the firstname and lastname as separated parameters of the person to look for
	 * @return the persons list match the provided first and lastname
	 */
	@GetMapping("/personInfo")
	@ResponseStatus(HttpStatus.FOUND)
	public MappingJacksonValue findPersonsByFullName(@RequestParam Map<String, String> allParams) throws EmptySearchResult {
		return personInfoService.findPersonsByFullName(allParams);
	}
}

package com.safetynet.alerts.controller;

import com.safetynet.alerts.exception.EmptySearchResult;
import com.safetynet.alerts.service.PhoneAlertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class PhoneAlertController {

	@Autowired
	PhoneAlertService phoneAlertService;

	/**
	 * Gets phone numbers by firestation number.
	 *
	 * @param firestationNumber the firestation number
	 * @return the phone numbers by firestation number
	 */
	@GetMapping("/phoneAlert")
	@ResponseStatus(HttpStatus.FOUND)
	public Set<String> getPhoneNumbersByFirestationNumber(@RequestParam(name = "firestation") String firestationNumber) throws EmptySearchResult {
		return phoneAlertService.findPhoneNumbersByStationNumber(firestationNumber);
	}
}

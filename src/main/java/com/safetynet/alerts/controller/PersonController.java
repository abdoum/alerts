package com.safetynet.alerts.controller;

import com.safetynet.alerts.model.PersonDTO;
import com.safetynet.alerts.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class PersonController {

	@Autowired
	PersonService personService;

	/**
	 * Save a person to database.
	 *
	 * @param person the person
	 * @return the list
	 */
	@PostMapping(value = "/person")
	@ResponseStatus(HttpStatus.CREATED)
	public PersonDTO add(@Valid @RequestBody PersonDTO person) {
		return personService.addPerson(person);
	}

	/**
	 * Update a person.
	 *
	 * @param person the person
	 * @return the list
	 */
	@PutMapping(value = "/person")
	public PersonDTO update(@RequestBody PersonDTO person) {
		return personService.updatePerson(person);
	}

	/**
	 * Delete a person.
	 *
	 * @param fullNameOfPerson the full name of person
	 */
	@DeleteMapping(value = "/person/{fullNameOfPerson}")
	public void delete(@PathVariable String fullNameOfPerson){
		personService.deletePerson(fullNameOfPerson);
	}

	/**
	 * Find children by address mapping jackson value.
	 *
	 * @param address the address
	 * @return the mapping jackson value
	 */
	@GetMapping(value = "/childAlert")

	public MappingJacksonValue findChildrenByAddress(@RequestParam(name = "address") String address) {
		return personService.getAllChildrenByAddress(address);
	}
}

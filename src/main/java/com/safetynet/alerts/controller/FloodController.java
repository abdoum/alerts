package com.safetynet.alerts.controller;

import com.safetynet.alerts.model.FloodItemWrapper;
import com.safetynet.alerts.service.FloodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FloodController {

	@Autowired
	FloodService floodService;

	/**
	 * Find houses covered by firestation map.
	 *
	 * @param listOfFirestationNumbers the list of firestation numbers
	 * @return the list of addresses covered by the stations and the persons living at these addresses
	 */
	@GetMapping("/flood/stations")
	@ResponseStatus(HttpStatus.FOUND)
	public List<FloodItemWrapper> findHousesCoveredByFirestation(@RequestParam(name = "stations") List<String> listOfFirestationNumbers) {
		return floodService.findCoveredResidents(listOfFirestationNumbers);
	}


}

package com.safetynet.alerts.controller;

import com.safetynet.alerts.model.FirestationDTO;
import com.safetynet.alerts.service.FirestationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class FirestationController {

	@Autowired
	FirestationService firestationService;

	/**
	 * Add list.
	 *
	 * @param firestation the fire station
	 * @return the list
	 */
	@PostMapping(value = "/firestation")
	@ResponseStatus(HttpStatus.CREATED)
	public FirestationDTO add(@Valid @RequestBody FirestationDTO firestation) {
		return firestationService.addFirestation(firestation);
	}

	/**
	 * Update firestation number.
	 *
	 * @param firestation the modified firestation
	 * @return the list
	 */
	@PatchMapping(value = "/firestation")
	public FirestationDTO update(@Valid @RequestBody FirestationDTO firestation) {
		return firestationService.updateFirestation(firestation);
	}

	/**
	 * Delete list.
	 *
	 * @param firestation the firestation to be deleted
	 */
	@DeleteMapping(value = "/firestation")
	public void delete(@Valid @RequestBody FirestationDTO firestation){
		firestationService.deleteFirestation(firestation);
	}

	/**
	 * Find covered persons by firestation number.
	 *
	 * @param stationNumber the station number
	 * @return the mapping jackson value
	 */
	@GetMapping(value = "/firestation")
	@ResponseStatus(HttpStatus.FOUND)
	public MappingJacksonValue findCoveredPersons(@RequestParam(name = "stationNumber") String stationNumber) {
		return firestationService.findCoveredPersonsByFirestationNumber(stationNumber);
	}
}

package com.safetynet.alerts.controller;

import com.safetynet.alerts.model.MedicalRecordDTO;
import com.safetynet.alerts.service.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class MedicalRecordController {

	@Autowired
	MedicalRecordService medicalRecordService;

	/**
	 * Add list.
	 *
	 * @param medicalRecord the medical record
	 * @return the list
	 */
	@PostMapping(value = "/medicalRecord")
	@ResponseStatus(HttpStatus.CREATED)
	public MedicalRecordDTO add(@RequestBody MedicalRecordDTO medicalRecord) {
		return medicalRecordService.addMedicalRecord(medicalRecord);
	}

	/**
	 * Update a medical record.
	 *
	 * @param medicalRecord the medical record
	 * @return the updated medical record
	 */
	@PutMapping(value = "/medicalRecord")
	public MedicalRecordDTO update(@RequestBody MedicalRecordDTO medicalRecord) {
		return medicalRecordService.updateMedicalRecord(medicalRecord);
	}

	/**
	 * Delete list.
	 *
	 * @param medicalRecordToDeleteFullName the medical record to delete full name
	 */
	@DeleteMapping(value = "/medicalRecord/{medicalRecordToDeleteFullName}")
	public void delete(@PathVariable String medicalRecordToDeleteFullName){
		medicalRecordService.deleteMedicalRecord(medicalRecordToDeleteFullName);
	}
}

package com.safetynet.alerts.controller;

import com.safetynet.alerts.model.FireDTO;
import com.safetynet.alerts.service.FireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FireController {

	@Autowired
	FireService fireService;

	/**
	 * Find persons and firestation coverage.
	 *
	 * @param address the address to look for
	 * @return the list of residents and stations for the provided address
	 */
	@GetMapping("/fire")
	@ResponseStatus(HttpStatus.FOUND)
	public List<FireDTO> findResidentsAndFirestationByAddress(@RequestParam(name = "address") String address) {
		return fireService.findResidentsAndFirestationByAddress(address);
	}


}

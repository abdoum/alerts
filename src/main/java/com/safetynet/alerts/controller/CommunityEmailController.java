package com.safetynet.alerts.controller;

import com.safetynet.alerts.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class CommunityEmailController {

	@Autowired
	PersonService personService;

	/**
	 * Gets persons emails for a given city.
	 *
	 * @param cityName the city name
	 * @return the persons emails for a city
	 */
	@GetMapping("/communityEmail")
	@ResponseStatus(HttpStatus.FOUND)
	public List<String> getPersonsEmailsForACity(@RequestParam(name = "city") String cityName) {
		return personService.getPersonsEmailsForACity(cityName);
	}


}

package com.safetynet.alerts.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "com.safetynet.alerts")
public class JacksonConfiguration {
	public JacksonConfiguration(ObjectMapper objectMapper) {
		objectMapper.setFilterProvider(new SimpleFilterProvider().setFailOnUnknownId(false));
	}
}

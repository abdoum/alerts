package com.safetynet.alerts.model;

import lombok.Data;

import java.util.List;

@Data
public class FloodItemWrapper {
	private String address;
	private List<FloodItemDTO> persons;
}

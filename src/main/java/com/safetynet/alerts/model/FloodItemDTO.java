package com.safetynet.alerts.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "flood")
public class FloodItemDTO {

	@Id
	@GeneratedValue
	@JsonIgnore
	private Long id;

	@JsonIgnore
	private String address;

	private String fullNameAndMedicalRecords;

	private String phone;

	private int age;

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public String getPerson() {
		return fullNameAndMedicalRecords;
	}

	public void setPerson(String fullNameAndMedicalRecords) {
		this.fullNameAndMedicalRecords = fullNameAndMedicalRecords;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Flood{" +
				"id=" + id +
				", address='" + address + '\'' +
				", person='" + fullNameAndMedicalRecords + '\'' +
				", phone='" + phone + '\'' +
				", age=" + age +
				'}';
	}
}

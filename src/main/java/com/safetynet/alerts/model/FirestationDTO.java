package com.safetynet.alerts.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@Entity
@Table(name = "firestation")
public class FirestationDTO {

	@Id
	@GeneratedValue
	@JsonIgnore
	private Long id;

	@Min(value=1)
	@Length(min=1)
	private String station;

	@Length(min=3, max=200)
	private String address;

	public Long getId() {
		return id;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}
}

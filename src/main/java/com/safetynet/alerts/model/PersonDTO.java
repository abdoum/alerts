package com.safetynet.alerts.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.Period;

@Slf4j
@Entity
@Table(name = "person")
@JsonFilter("person")
public class PersonDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private Long id;

	@NotBlank
	private String firstName;

	@NotBlank
	private String lastName;

	@Length(min=3)
	@NotNull
	private String address;

	@NotBlank
	private String city;

	@NotBlank
	private String zip;

	@Length(min=9)
	private String phone;

	@Email
	private String email;

	@DateTimeFormat
	private LocalDate birthdate;

	@NumberFormat
	private int age;

	@OneToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@Nullable
	private MedicalRecordDTO medicalRecord;

	public Integer getAge() {
		Integer ageResult = null;
		try {
			var diff = Period.between(this.getBirthdate(), LocalDate.now());
			ageResult = diff.getYears();

		} catch (Exception e) {
			log.warn("no medical record found for " + birthdate);
		}
		return ageResult;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public Long getId() {
		return id;
	}

	@JsonIgnore
	public String getFullName() {
		return this.getFirstName().concat(getLastName());
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {

		this.firstName = StringUtils.capitalize(firstName.toLowerCase().trim());
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = StringUtils.capitalize(lastName.toLowerCase().trim());
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = StringUtils.capitalize(address.toLowerCase().trim());
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = StringUtils.capitalize(city.toLowerCase().trim());
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Nullable
	public MedicalRecordDTO getMedicalRecord() {
		return medicalRecord;
	}

	public void setMedicalRecord(@Nullable MedicalRecordDTO medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

	@Override
	public String toString() {
		return "PersonDTO{" +
				"id=" + id +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", address='" + address + '\'' +
				", city='" + city + '\'' +
				", zip='" + zip + '\'' +
				", phone='" + phone + '\'' +
				", email='" + email + '\'' +
				", birthdate=" + birthdate +
				", age=" + age +
				", medicalRecord=" + medicalRecord +
				'}';
	}
}

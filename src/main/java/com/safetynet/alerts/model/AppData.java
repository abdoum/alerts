package com.safetynet.alerts.model;

import lombok.Data;

import java.util.List;

@Data
public class AppData {

	private List<FirestationDTO> firestations;
	private List<MedicalRecordDTO> medicalrecords;
	private List<PersonDTO> persons;
}

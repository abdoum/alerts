package com.safetynet.alerts.model;

import lombok.Data;

import java.util.List;

@Data
public class ChildAndPersonsInHouse {

	private String firstName;
	private String lastName;
	private int age;
	private List<PersonDTO> otherPersonsInTheHouse;
}

package com.safetynet.alerts.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "medicalRecord")
public class MedicalRecordDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private Integer id;

	@Length(min=1)
	private String firstName;

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Length(min=1)
	private String lastName;

	@DateTimeFormat
	private String birthdate;

	@Nullable
	@ElementCollection
	private List<String> medications;

	@Nullable
	@ElementCollection
	private List<String> allergies;

	public String getFullName() {

		return firstName + lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	@Nullable
	public List<String> getMedications() {
		return medications;
	}

	public void setMedications(@Nullable List<String> medications) {
		this.medications = medications;
	}

	@Nullable
	public List<String> getAllergies() {
		return allergies;
	}

	public void setAllergies(@Nullable List<String> allergies) {
		this.allergies = allergies;
	}

	@Override
	public String toString() {
		return "MedicalRecordDTO{" +
				"id=" + id +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", birthdate='" + birthdate + '\'' +
				", medications=" + medications +
				", allergies=" + allergies +
				'}';
	}
}

package com.safetynet.alerts.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "fire")
public class FireDTO {

	@Id
	@GeneratedValue
	@JsonIgnore
	private Long id;

	private String fullName;

	private String phone;

	private int age;

	@OneToOne(targetEntity=MedicalRecordOnly.class, fetch=FetchType.EAGER)
	@Nullable
	@NotFound(action = NotFoundAction.IGNORE)
	private MedicalRecordOnly medicalRecord;

	@ElementCollection
	private List<String> fireStationNumber;

	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public MedicalRecordOnly getMedicalRecord() {
		return medicalRecord;
	}
	public void setMedicalRecord(MedicalRecordOnly medicalRecord) {
		this.medicalRecord = medicalRecord;
	}
	public List<String> getFireStationNumber() {
		return fireStationNumber;
	}
	public void setFireStationNumber(List<String> fireStationNumber) {
		this.fireStationNumber = fireStationNumber;
	}

	@Override
	public String toString() {
		return "FireDTO{" +
				"id=" + id +
				", fullName='" + fullName + '\'' +
				", phone='" + phone + '\'' +
				", age=" + age +
				", medicalRecord=" + medicalRecord +
				", fireStationNumber=" + fireStationNumber +
				'}';
	}
}

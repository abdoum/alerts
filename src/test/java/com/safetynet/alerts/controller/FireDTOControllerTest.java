package com.safetynet.alerts.controller;

import com.safetynet.alerts.model.FireDTO;
import com.safetynet.alerts.model.MedicalRecordOnly;
import com.safetynet.alerts.model.PersonDTO;
import com.safetynet.alerts.repository.IFireDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class FireDTOControllerTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	IFireDao iFireDao;

	@Test
	void testFindPersonsAndFirestationCoverage() throws Exception {
		FireDTO fireDTO = new FireDTO();
		fireDTO.setFullName("AlexRoad");
		fireDTO.setAge(56);
		MedicalRecordOnly medicalRecordOnly = new MedicalRecordOnly();
		medicalRecordOnly.setAllergies(Arrays.asList("pollen", "walnuts"));
		medicalRecordOnly.setMedications(Arrays.asList("tradoxidine:400mg", "dafalgan"));
		fireDTO.setFireStationNumber(new ArrayList<>(Arrays.asList("1", "2")));
		fireDTO.setMedicalRecord(medicalRecordOnly);
		fireDTO.setPhone("456-784-9788");

		PersonDTO person = new PersonDTO();
		person.setFirstName("alex");
		person.setLastName("Road");
		person.setPhone(fireDTO.getPhone());
		var personList = Arrays.asList(person);

		when(iFireDao.getAllByAddress("112 Steppes Pl")).thenReturn(Collections.singletonList(fireDTO));

		mockMvc.perform(get("/fire")
				.param("address", "112 Steppes Pl"))
				.andDo(print())
				.andExpect(status().isFound())
				.andExpect(jsonPath("$[0].fullName", is(person.getFullName())))
				.andExpect(jsonPath("$[0].medicalRecord.medications", is(fireDTO.getMedicalRecord().getMedications())))
				.andExpect(jsonPath("$[0].fireStationNumber[0]", is("1")))
				.andExpect(jsonPath("$[0].phone", is(person.getPhone())));
	}
}

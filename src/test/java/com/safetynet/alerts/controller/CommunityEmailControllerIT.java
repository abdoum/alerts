package com.safetynet.alerts.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CommunityEmailControllerIT {

	@Autowired
	MockMvc mockMvc;

	@Test
	@DisplayName("get emails for a city : should return an array of emails for a valid city name")
	void testGetPersonsEmailsForACity_shouldReturnAnArrayOfEmails_forAValidCity() throws Exception {
		mockMvc.perform(get("/communityEmail")
				.param("city", "  CulVer  "))
				.andExpect(status().isFound())
				.andExpect(jsonPath("$[0]", is("aly@imail.com")))
				.andExpect(jsonPath("$").isArray());
	}

	@Test
	@DisplayName("get emails for a city : should return http 404 status for a non existing city name")
	void testGetPersonsEmailsForACity_shouldReturnHttpNotFound_forAnInvalidCity() throws Exception {
		mockMvc.perform(get("/communityEmail")
				.param("city", "  CulVeer "))
				.andExpect(status().isNotFound());
	}
}

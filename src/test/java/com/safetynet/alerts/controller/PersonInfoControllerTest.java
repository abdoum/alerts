package com.safetynet.alerts.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PersonInfoControllerTest {

	@Autowired
	MockMvc mockMvc;

	@Test
	void testFindPersonsByFullName() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
			params.put("firstName", Collections.singletonList("Lilly"));
			params.put("lastName", Collections.singletonList("Cooper"));
		mockMvc.perform(get("/personInfo")
				.params(params))
				.andDo(print())
				.andExpect(content().string("[{\"firstName\":\"Tony\",\"lastName\":\"Cooper\",\"address\":\"112 Steppes Pl\",\"email\":\"tcoop@ymail.com\",\"age\":27,\"medicalRecord\":{\"medications\":[\"hydrapermazol:300mg\",\"dodoxadin:30mg\"],\"allergies\":[\"shellfish\"]}},{\"firstName\":\"Lily\",\"lastName\":\"Cooper\",\"address\":\"489 Manchester St\",\"email\":\"lily@email.com\",\"age\":27,\"medicalRecord\":{\"medications\":[],\"allergies\":[]}}]"))
				.andExpect(status().isOk());
	}

	@Test
	void testFindPersonsByFullName_shouldThrowException_forEmptySearchResults() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
			params.put("firstName", Collections.singletonList("x"));
			params.put("lastName", Collections.singletonList("x"));
		mockMvc.perform(get("/personInfo")
				.params(params))
				.andDo(print())
				.andExpect(status().isNotFound());
	}
}

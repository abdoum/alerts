package com.safetynet.alerts.controller;

import com.safetynet.alerts.model.MedicalRecordDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;

import static com.safetynet.alerts.controller.PersonControllerTest.asJsonString;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class MedicalRecordControllerIT {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	MedicalRecordDTO existingMedicalRecord;

	@BeforeEach
	void setUp() {
		ArrayList<String> allergies = new ArrayList<>(Arrays.asList("pollen", "peanuts"));
		ArrayList<String> medications = new ArrayList<>(Arrays.asList("doliprane", "dafalgan"));

		existingMedicalRecord = new MedicalRecordDTO();
		existingMedicalRecord.setBirthdate("02/18/2012\"");
		existingMedicalRecord.setFirstName("Tessa");
		existingMedicalRecord.setLastName("Carman");
		existingMedicalRecord.setAllergies(allergies);
		existingMedicalRecord.setMedications(medications);
	}

	@Test
	void testAddMedicalRecord() throws Exception {
		existingMedicalRecord.setFirstName("  ABy  ");
		existingMedicalRecord.setLastName("  jaCk  ");
		mockMvc.perform(post("/medicalRecord")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(existingMedicalRecord)))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(jsonPath("$[-1].firstName", is("Aby")))
				.andExpect(jsonPath("$[-1].lastName", is("Jack")));
	}

	@Test
	void testUpdateMedicalRecord_shouldReturn404_forANonExistingRecord() throws Exception {
		existingMedicalRecord.setLastName("xxx");
		mockMvc.perform(put("/medicalRecord")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(existingMedicalRecord)))
				.andExpect(status().isBadRequest())
				.andDo(print());
	}

	@Test
	void testUpdateMedicalRecord() throws Exception {
		mockMvc.perform(put("/medicalRecord")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(existingMedicalRecord)))
				.andExpect(status().isOk())
				.andDo(print());
	}

	@Test
	void testDeleteMedicalRecord_shouldReturn200_forAnExistingRecord() throws Exception {
		mockMvc.perform(delete("/medicalRecord/{medicalRecordToDeleteFullName}","  TessACarman  "))
			.andExpect(status().isOk())
			.andDo(print());
	}

	@Test
	void testDeleteMedicalRecord_shouldReturn404_forANonExistingMedicalRecord() throws Exception {
		mockMvc.perform(delete("/medicalRecord/{medicalRecordToDeleteFullName}"," xxxx "))
			.andExpect(status().isNotFound())
			.andDo(print());
	}
}

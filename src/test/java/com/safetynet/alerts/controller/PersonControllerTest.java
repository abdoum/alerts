package com.safetynet.alerts.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.safetynet.alerts.model.MedicalRecordDTO;
import com.safetynet.alerts.model.PersonDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PersonControllerTest {

	@Autowired
	public MockMvc mockMvc;

	@MockBean
	PersonDTO person;

	@MockBean
	MedicalRecordDTO medicalRecord;

	public static String asJsonString(final Object obj) {
		try {
			SimpleFilterProvider filters = new SimpleFilterProvider();
			filters.setFailOnUnknownId(false);
			return new ObjectMapper().writer(filters).writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@BeforeEach
	void setUp() {
		ArrayList<String> allergies = new ArrayList<String>(Arrays.asList("pollen", "peanuts"));
		ArrayList<String> medications = new ArrayList<String>(Arrays.asList("doliprane", "dafalgan"));
		medicalRecord = new MedicalRecordDTO();
		medicalRecord.setBirthdate("03/22/2000");
		medicalRecord.setFirstName("Ahmad");
		medicalRecord.setLastName("Chater");
		medicalRecord.setAllergies(allergies);
		medicalRecord.setMedications(medications);
		person = new PersonDTO();
		person.setFirstName("Ahmad");
		person.setLastName("Chater");
		person.setAge(18);
		person.setCity("Denver");
		person.setPhone("55-065-0151");
		person.setEmail("email@emao.com");
		person.setZip("18484");
		person.setAddress("1554 denver St.");
		person.setMedicalRecord(medicalRecord);
		SimpleFilterProvider filters = new SimpleFilterProvider();
		filters.setFailOnUnknownId(false);
	}

	@Test
	void testAddPerson() throws Exception {
		mockMvc.perform(post("/person")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(person)))
            .andExpect(status().isCreated());
	}

	@Test
	void testUpdatePerson() throws Exception {
		mockMvc.perform(put("/person")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(person)))
				.andExpect(status().isNoContent());
	}

	@Test
	void testDeleteDeletePerson() throws Exception {
		mockMvc.perform(delete("/person/{fullNameOfPerson}", "TessaCarman"))
				.andExpect(status().isNoContent());
	}

	@Test
	void testFindChildrenByAddress() throws Exception {
		mockMvc.perform(get("/childAlert")
				.param("address","1509 Culver St" ))
				.andDo(print())
				.andExpect(status().isFound());
	}

}

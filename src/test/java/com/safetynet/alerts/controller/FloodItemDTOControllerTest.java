package com.safetynet.alerts.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class FloodItemDTOControllerTest {

	@Autowired
	MockMvc mockMvc;


	@Test
	void testFindHousesCoveredByFirestation() throws Exception {
	mockMvc.perform(get("/flood/stations")
			.param("stations", "1,2,3"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$[0]").exists());
	}
}

package com.safetynet.alerts.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PhoneAlertControllerIT {

	@Autowired
	MockMvc mockMvc;

	@Test
	void testGetPhoneNumbersByFirestationNumber() throws Exception {
		mockMvc.perform(get("/phoneAlert")
				.param("firestation", "1"))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(jsonPath("$[0]").exists())
				.andExpect(jsonPath("$[0]", is("841-874-7784")))
				.andExpect(jsonPath("$[-1]", is("841-874-8547")));

	}

	@Test
	void testGetPhoneNumbersByFirestationNumber_shouldReturn404Error_forAnInvalidFirestationNumber() throws Exception {
		mockMvc.perform(get("/phoneAlert")
				.param("firestation", "x"))
				.andDo(print())
				.andExpect(status().isNotFound());
	}
}

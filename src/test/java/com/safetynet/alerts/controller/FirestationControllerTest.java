package com.safetynet.alerts.controller;

import com.safetynet.alerts.model.FirestationDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static com.safetynet.alerts.controller.PersonControllerTest.asJsonString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class FirestationControllerIT {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	FirestationDTO firestation;

	@BeforeEach
	void setUp() {
		firestation = new FirestationDTO();
		firestation.setAddress("1 rue de la concorde");
		firestation.setStation("99");
	}

	@Test
	void testAddFirestation() throws Exception {
		mockMvc.perform(post("/firestation")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(firestation)))
				.andExpect(status().isCreated());
	}

	@Test
	void testUpdateAFirestationNumber() throws Exception {
		firestation.setStation("100");
		mockMvc.perform(patch("/firestation")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(firestation)))
				.andExpect(status().isOk());
	}

	@Test
	void testDeleteFirestation() throws Exception {
		mockMvc.perform(delete("/firestation")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(firestation)))
				.andExpect(status().isOk());
	}

	@Test
	void testFindCoveredPersonsByFirestationNumber() throws Exception {
		mockMvc.perform(get("/firestation")
				.param("stationNumber","4" ))
				.andExpect(status().isFound())
				.andExpect(jsonPath("$[0].firstName", is("Tony")));
	}
}

package com.safetynet.alerts.controller;

import com.safetynet.alerts.model.MedicalRecordDTO;
import com.safetynet.alerts.model.PersonDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;

import static com.safetynet.alerts.controller.PersonControllerTest.asJsonString;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class PersonControllerIT {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	PersonDTO person;

	@MockBean
	MedicalRecordDTO medicalRecord;

	@BeforeEach
	void setUp() {
		ArrayList<String> allergies = new ArrayList<String>(Arrays.asList("pollen", "peanuts"));
		ArrayList<String> medications = new ArrayList<String>(Arrays.asList("doliprane", "dafalgan"));
		medicalRecord = new MedicalRecordDTO();
		medicalRecord.setBirthdate("03/22/2000");
		medicalRecord.setFirstName("Ahmad");
		medicalRecord.setLastName("Chater");
		medicalRecord.setAllergies(allergies);
		medicalRecord.setMedications(medications);
		person = new PersonDTO();
		person.setFirstName("Ahmad");
		person.setLastName("Chater");
		person.setAge(18);
		person.setCity("Denver");
		person.setPhone("55-065-0151");
		person.setEmail("email@emao.com");
		person.setZip("18484");
		person.setAddress("1554 denver St.");
		person.setMedicalRecord(medicalRecord);

//		existingMedicalRecord = new MedicalRecord();
//		existingMedicalRecord.setBirthdate("02/18/2012\"");
//		existingMedicalRecord.setFirstName("Tessa");
//		existingMedicalRecord.setLastName("Carman");
//		existingMedicalRecord.setAllergies(new ArrayList<String>());
//		existingMedicalRecord.setMedications(new ArrayList<String>());
//		existingPerson = new Person();
//		existingPerson.setFirstName("Tessa");
//		existingPerson.setLastName("Carman");
//		existingPerson.setCity("Culver");
//		existingPerson.setPhone("841-874-6512");
//		existingPerson.setEmail("tenz@email.com");
//		existingPerson.setZip("97451");
//		existingPerson.setAddress("34 Binoc Ave");
//		existingPerson.setMedicalRecord(existingMedicalRecord);
	}

	@Test
	void testAdd() throws Exception {
		mockMvc.perform(post("/person")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(person)))
				.andExpect(status().isCreated());
	}

	@Test
	void testFindChildrenByAddress_shouldReturnTheExpectedJsonResponse_forTheGivenAddress() throws Exception {
		mockMvc.perform(get("/childAlert")
				.param("address","1509 Culver St" ))
				.andDo(print())
				.andExpect(status().isFound())
				.andExpect( content().contentType("application/json"))
				.andExpect(jsonPath("$[0].firstName", is("Roger")))
				.andExpect(jsonPath("$[0].lastName", is("Boyd")))
				.andExpect(jsonPath("$[0].age", is(3)))
				.andExpect(jsonPath("$[0].otherPersonsInTheHouse").isArray())
				.andExpect(jsonPath("$[0].otherPersonsInTheHouse[0].firstName", is("John")))
				.andExpect(jsonPath("$[0].otherPersonsInTheHouse[0].lastName").exists());
	}
}
